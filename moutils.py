import copy
import logging
import os
import threading

def setloglevel(logger):
	formatter = logging.Formatter('[%(asctime)s] %(levelname)s %(message)s')
	streamlogger = logging.StreamHandler()
	streamlogger.setFormatter(formatter)
	logger.addHandler(streamlogger)

	if os.environ.get('LOG_LEVEL') is None:
		return

	logger.setLevel(os.environ.get('LOG_LEVEL'))


def externalhostport():
	return os.environ.get('EXTERNAL_HOSTPORT', 'localhost')


def webhost():
	return os.environ.get('WEB_HOST', '0.0.0.0')


def webport():
	port = os.environ.get('WEB_PORT')
	if port is not None:
		return int(port)
	else:
		return int(os.environ['PORT'])


def redishost():
	return os.environ['REDIS_HOST']


def redisport():
	return int(os.environ['REDIS_PORT'])


def redispassword():
	return os.environ['REDIS_PASSWORD']


def memcachedhost():
	host = os.environ.get('MEMCACHED_HOST')
	if host is not None:
		return host
	else:
		return os.environ['MEMCACHIER_SERVERS']


def memcachedusername():
	host = os.environ.get('MEMCACHED_HOST')
	if host is not None:
		return None
	else:
		return os.environ['MEMCACHIER_USERNAME']


def memcachedpassword():
	host = os.environ.get('MEMCACHED_HOST')
	if host is not None:
		return None
	else:
		return os.environ['MEMCACHIER_PASSWORD']


def reloader():
	return bool(int(os.environ.get('WEB_RELOAD', 0)))


def isempty(s):
	if s is None:
		return True

	if s == '':
		return True

	if s == 0:
		return True

	if hasattr(s, '__len__'):
		return len(s) == 0

	return False


def allempty(s, *l):
	if not isempty(s):
		return False

	for e in l:
		if not isempty(e):
			return False
	
	return True


def notempty(s):
	return not isempty(s)


def allnotempty(s, *l):
	if isempty(s):
		return False

	for e in l:
		if isempty(e):
			return False
	
	return True


def anyempty(s, *l):
	return not allnotempty(s, *l)


def strorempty(s):
    if isempty(s):
        return ''
    else:
        return s


def safetrim(s):
	if s is None:
		return '' 

	return s.strip()


def safedel(d, key=None, keys=None):
	try:
		del d[key]
	except:
		pass
	
	if keys is not None:
		[safedel(d, each) for each in keys]

	return d


def setnotempty(d, key, value):
	if d is None or isempty(key) or isempty(value):
		return

	d[key] = value


#Inspired by Objective C's -valueForKeyPath:
def valueforkeypath(d, path, default=None):
	paths = path.split('.')
	for each in paths:
		d = d.get(each)
		if d is None:
			return default
	
	return d


#Return first item in sequence where f(item) == True.
def find(f, seq, default=None):
	for item in seq:
		if f(item):
			return item

	return default


#Returns which keys don't exist in the dictionary or have empty values as a list
def missingkeys(d, keys):
	result = []
	for k in keys:
		if isempty(d.get(k)):
			result.append(k)
	return result


#Courtesy http://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
class memoized(object):
	'''Decorator. Caches a function's return value each time it is called.  If called later with the same arguments, the cached value is returned (not reevaluated).'''
	def __init__(self, func):
		self.func = func
		self.cache = {}


	def __call__(self, *args):
		try:
			return self.cache[args]
		except KeyError:
			value = self.func(*args)
			self.cache[args] = value
			return value
		except TypeError:
			# uncachable -- for instance, passing a list as an argument.
			# Better to not cache than to blow up entirely.
			return self.func(*args)


		def __repr__(self):
			'''Return the function's docstring.'''
			return self.func.__doc__


		def __get__(self, obj, objtype):
			'''Support instance methods.'''
			return functools.partial(self.__call__, obj)


# Keys can be dot separated to represent multi-level dictionaries
def combinedictionaries(source_dict, newkeyvalues):
	new = copy.deepcopy(source_dict)
	for k,v in newkeyvalues.items():
		d = new
		parts = k.split(".")
		for p in parts[:-1]:
			d = d.setdefault(p, {})
		d[parts[-1]] = v
	
	return new


#PSF license
## {{{ http://code.activestate.com/recipes/84317/ (r2)
class Future:
    def __init__(self,func,*param):
        # Constructor
        self.__done=0
        self.__result=None
        self.__status='working'

        self.__C=threading.Condition()   # Notify on this Condition when result is ready

        # Run the actual function in a separate thread
        self.__T=threading.Thread(target=self.Wrapper,args=(func,param))
        self.__T.setName('FutureThread')
        self.__T.start()

    def __repr__(self):
        return '<Future at '+hex(id(self))+':'+self.__status+'>'

    def __call__(self):
        self.__C.acquire()
        while self.__done==0:
            self.__C.wait()
        self.__C.release()
        # We deepcopy __result to prevent accidental tampering with it.
        a=copy.deepcopy(self.__result)
        return a

    def Wrapper(self, func, param):
        # Run the actual function, and let us housekeep around it
        self.__C.acquire()
        try:
            self.__result=func(*param)
        except:
            self.__result='Exception raised within Future'
        self.__done=1
        self.__status=`self.__result`
        self.__C.notify()
        self.__C.release()
## end of http://code.activestate.com/recipes/84317/ }}}


def findkey(obj, key, prefix='', results=None):
	if results is None:
		results = set()
	if isinstance(obj, list):
		for e in obj:
			findkey(e, key, '{}[]'.format(prefix), results)
	elif isinstance(obj, dict):
		for k,v in obj.items():
			if k == key:
				results.add('{}[{}]'.format(prefix, key))
			else:
				findkey(v, key, '{}[{}]'.format(prefix, k), results)
	return results


def findvalueforkey(obj, key, prefix='', results=None):
	if results is None:
		results = []
	if isinstance(obj, list):
		for e in obj:
			findvalueforkey(e, key, '{}[]'.format(prefix), results)
	elif isinstance(obj, dict):
		for k,v in obj.items():
			if k == key:
				results.append(v)
			else:
				findvalueforkey(v, key, '{}[{}]'.format(prefix, k), results)
	return results
